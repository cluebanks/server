import inject
import json
from flask import Flask, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import import_string
from werkzeug.exceptions import HTTPException
from flasgger import Swagger

from .my_encoder import MyEncoder
from .my_exceptions import ClueBanksException

app = Flask(__name__)
app.config.from_object(import_string(f'cluebanks.{app.env}_config.Config')())
app.json_encoder = MyEncoder

swagger = Swagger(app)

@app.errorhandler(Exception)
def handle_exception(e):
    if isinstance(e, HTTPException):
        code=e.code
        description=str(e)
    elif isinstance(e, ClueBanksException):
        code=400
        description=str(e)
    else:
        code=500
        description=str(e) if app.env=='development' else '**********'

    response = make_response(json.dumps({
        "code": code,
        "error": type(e).__name__,
        "description": description 
    }), code)
    response.content_type = "application/json"
    return response



db = SQLAlchemy(app)
db.create_all()


from .mod_accounts.account_service import AccountService
from .mod_users.user_service import UserService
from .geocoder.geocoder_service import GeocoderService
def binds(binder):
    binder.bind(SQLAlchemy, db)
    binder.bind(GeocoderService, GeocoderService())
    binder.bind(UserService, UserService())
    binder.bind(AccountService, AccountService())

inject.configure(binds)


from .mod_users.user_controller import mod_users
from .mod_accounts.account_controller import mod_accounts
app.register_blueprint(mod_users)
app.register_blueprint(mod_accounts)
