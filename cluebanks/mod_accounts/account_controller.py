import inject
from flask import jsonify, request
from flask_sqlalchemy import SQLAlchemy

from . import mod_accounts
from .account_service import AccountService
from .account_schemas import CreateAccountSchema, AmountSchema

@mod_accounts.route('', methods=['GET'])
@inject.autoparams()
def get_accounts(account_service: AccountService):
    """Get list of accounts.
    ---
    parameters:
      - name: offset
        in: query
        required: false
        type: number
      - name: limit
        in: query
        required: false
        type: number
    tags:
      - Accounts
    responses:
      200:
        description: A list of accounts 
    """
    return jsonify(account_service.get_accounts(offset=request.args.get('offset', type=int), limit=request.args.get('limit', type=int)))

@mod_accounts.route('/<number>', methods=['GET'])
@inject.autoparams()
def get_account(number:str, account_service: AccountService):
    """Get account details.
    ---
    tags:
      - Accounts
    parameters:
      - name: id
        in: path
        required: true
        type: number
    responses:
      200:
        description: A detailed account
    """
    return jsonify(account_service.get_account(number))

@mod_accounts.route('', methods=['POST'])
@inject.autoparams()
def post_account(account_service: AccountService):
    """
    Create account
    ---
    tags:
      - Accounts
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                user_id:
                    type: number
                    description: The id of the account's owner.
                account_type:
                    type: string
                    enum: ['silver', 'gold', 'platinium']
                    description: The account type.

    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The account inserted in the database
    """    
    return jsonify(account_service.post_account(CreateAccountSchema(**request.json)))    

@mod_accounts.route('/<number>/credit', methods=['POST'])
@inject.autoparams()
def credit_account(number:str, account_service: AccountService):
    """
    Credit an account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string
        description: The account's number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be credited.
    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The updated account
    """    
    return jsonify(account_service.credit_account(number, AmountSchema(**request.json)))    

@mod_accounts.route('/<number>/debit', methods=['POST'])
@inject.autoparams()
def debit_account(number:str, account_service: AccountService):
    """
    Debit an account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string
        description: The account's number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be debited.
    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The updated account
    """    
    return jsonify(account_service.debit_account(number, AmountSchema(**request.json)))    

@mod_accounts.route('/<number_from>/transfer-to/<number_to>', methods=['POST'])
@inject.autoparams()
def tranfer_to(number_from:str, number_to:str, account_service: AccountService):
    """
    Transfert money to a different account
    ---
    tags:
      - Accounts
    parameters:
      - name: number_from
        in: path
        required: true
        type: string
        description: The origin account number.
      - name: number_to
        in: path
        required: true
        type: string
        description: The destination account number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be debited.
    responses:
      200:
        schema:
          type: object
          properties:
            debitedAccount:
              type: object
              properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
            creditedAccount:
              type: object
              properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: Debited and credited accounts
    """    
    return jsonify(account_service.transfer_account(number_from=number_from, number_to=number_to, transfer=AmountSchema(**request.json)))    

@mod_accounts.route('/<number>', methods=['DELETE'])
@inject.autoparams()
def delete_account(number:str, account_service: AccountService):
    """
    Delete account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string

    responses:
      200:
        schema:
            type: object
            properties:
                deletedRows:
                    type: number
                    description: The number of deleted rows.
        description: The number of deleted rows.
    """
    return jsonify(account_service.delete_account(number))        
