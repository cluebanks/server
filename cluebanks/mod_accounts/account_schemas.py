from typing import List
from pydantic import BaseModel

from .account_type import AccountType

class CreateAccountSchema(BaseModel):
    account_type: AccountType
    user_id: int

class AmountSchema(BaseModel):
    amount: int
 




