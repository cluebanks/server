from sqlalchemy import Column, Enum, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin

from .account_type import AccountType

from sqlalchemy.ext.declarative import declarative_base
from .. import db

class AccountModel(db.Model, SerializerMixin):
    __tablename__ = 'account'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'balance', 'user', 'number', 'account_type', 'user.firstname', 'user.lastname')
    serialize_rules = ('-user.accounts',)

    id          : int = Column(Integer, primary_key=True)
    balance     : int = Column(Integer, nullable=False)
    number      : str = Column(String(36), nullable=False, index=True, unique=True)
    account_type: AccountType = Column('account_type', Enum(AccountType))
    user_id     : int = Column(Integer, ForeignKey('user.id', ondelete="cascade"))

    user        = relationship("UserModel", back_populates="accounts")
