import uuid
from typing import Union

import inject
from typeguard import typechecked
from flask_sqlalchemy import SQLAlchemy

from .account_model import AccountModel
from .account_schemas import CreateAccountSchema, AmountSchema
from ..my_exceptions import ClueBanksException

class AccountService:
    db = inject.attr(SQLAlchemy)

    @typechecked
    def get_accounts(self, offset:Union[int,None], limit:Union[int,None]):
        return self.db.session.query(AccountModel).offset(offset).limit(limit).all()

    @typechecked
    def get_account(self, number:str):
        return self.db.session.query(AccountModel).filter(AccountModel.number==number).one()

    @typechecked
    def post_account(self, account: CreateAccountSchema):
        account = AccountModel(**account.__dict__)  
        account.balance=0
        account.number=str(uuid.uuid4())
        self.db.session.add(account)
        self.db.session.commit()
        return account

    @typechecked
    def credit_account(self, number: str, credit: AmountSchema):
        if (credit.amount<=0):
            raise ClueBanksException('Amount must be stricly positive')
        account = self.db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
        account.balance+=credit.amount
        self.db.session.add(account)
        self.db.session.commit()
        return account

    @typechecked
    def debit_account(self, number: str, debit: AmountSchema):
        if (debit.amount<=0):
            raise ClueBanksException('Amount must be stricly positive')
        account = self.db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
        if (account.balance<debit.amount):
            raise ClueBanksException('Not enough credit')
        account.balance-=debit.amount
        self.db.session.add(account)
        self.db.session.commit()
        return account

    @typechecked
    def transfer_account(self, number_from: str, number_to: str, transfer: AmountSchema):
        if (transfer.amount<=0):
            raise ClueBanksException('Amount must be stricly positive')
        if (number_from==number_to):
            raise ClueBanksException('Origin account must be different than destination account')
        account_from = self.db.session.query(AccountModel).filter(AccountModel.number==number_from).with_for_update().one()
        account_to = self.db.session.query(AccountModel).filter(AccountModel.number==number_to).with_for_update().one()
        if (account_from.balance<transfer.amount):
            raise ClueBanksException('Not enough credit')
        account_from.balance-=transfer.amount
        account_to.balance+=transfer.amount
        self.db.session.add(account_from)
        self.db.session.add(account_to)
        self.db.session.commit()
        return {"debited_account": account_from, "credited_account": account_to}

    @typechecked
    def delete_account(self, number: str):
        account = self.db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
        if account.balance>0:
            raise ClueBanksException('Account is not empty')
        deletedRows=self.db.session.query(AccountModel).filter(AccountModel.number==number).delete()
        self.db.session.commit()
        return {"deleted_rows":deletedRows}

