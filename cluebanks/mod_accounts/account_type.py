import enum

class AccountType(enum.Enum):
    silver = "silver"
    gold = "gold"
    platinium = "platinium"