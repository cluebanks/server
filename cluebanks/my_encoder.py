import json

from sqlalchemy_serializer import SerializerMixin


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, SerializerMixin):
            return obj.to_dict()
        return json.JSONEncoder.default(self, obj)
