from typing import Optional
from pydantic import BaseModel
import datetime


class CreateUserSchema(BaseModel):
    firstname   : str
    lastname    : str
    address     : Optional[str]
    birthdate   : Optional[datetime.date]

class SetUserSchema(BaseModel):
    firstname   : Optional[str]
    lastname    : Optional[str]
    address     : Optional[str]
    birthdate   : Optional[datetime.date]

