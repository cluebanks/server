import datetime
import json

from sqlalchemy import Column, ForeignKey, Integer, String, Float, Date
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin

from .. import db

from sqlalchemy.ext.declarative import declarative_base


class UserModel(db.Model, SerializerMixin):
    __tablename__ = 'user'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'firstname', 'lastname', 'address', 'lat', 'lng', 'birthdate', 'accounts.id', 'accounts.number', 'accounts.balance')
    serialize_rules = ('-accounts.user',)

    id          : int = Column(Integer, primary_key=True)
    firstname   : str = Column(String(128), nullable=False)
    lastname    : str = Column(String(128), nullable=False)
    address     : str = Column(String(256), nullable=True)
    lat         : float = Column(Float(), nullable=True)
    lng         : float = Column(Float(), nullable=True)
    birthdate   : datetime.date = Column(Date(), nullable=True)

    accounts    = relationship("AccountModel", back_populates="user", lazy='joined')
