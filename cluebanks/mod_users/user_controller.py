import inject
from flask import jsonify, request
from flask_sqlalchemy import SQLAlchemy

from . import mod_users
from .user_service import UserService
from .user_schemas import CreateUserSchema, SetUserSchema

@mod_users.route('', methods=['GET'])
@inject.autoparams()
def get_users(user_service: UserService):
    """Get list of users.
    ---
    parameters:
      - name: offset
        in: query
        required: false
        type: number
      - name: limit
        in: query
        required: false
        type: number
    tags:
      - Users
    responses:
      200:
        description: A list of users 
    """
    return jsonify(user_service.list(offset=request.args.get('offset', type=int), limit=request.args.get('limit', type=int)))

@mod_users.route('/<int:id>', methods=['GET'])
@inject.autoparams()
def get_user(id:int, user_service: UserService):
    """Get user details.
    ---
    tags:
      - Users
    parameters:
      - name: id
        in: path
        required: true
        type: number
    responses:
      200:
        description: A detailed user
    """
    return jsonify(user_service.get(id))
     
    
@mod_users.route('', methods=['POST'])
@inject.autoparams()
def post_user(user_service: UserService, db: SQLAlchemy):
    """
    Create user
    ---
    tags:
      - Users
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.
                birthdate:
                    type: string
                    description: The user's birthdate ('YYYY-MM-DD').
    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The user's id.
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.
        description: The user inserted in the database
    """
    user=user_service.create(CreateUserSchema(**request.json))
    db.session.commit()
    return jsonify(user)    

@mod_users.route('/<int:id>', methods=['PUT'])
@inject.autoparams()
def put_user(id:int, user_service: UserService, db: SQLAlchemy):
    """
    Update user
    ---
    tags:
      - Users
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.
                birthdate:
                    type: string
                    description: The user's birthdate ('YYYY-MM-DD').
      - name: id
        in: path
        required: true
        type: number

    responses:
      200:
        schema:
            type: object
            properties:
                updatedRows:
                    type: number
                    description: The number of updated rows.
        description: The number of updated rows.
    """
    updated_rows=user_service.set(id, SetUserSchema(**request.json))
    db.session.commit()
    return jsonify({"updated_rows":updated_rows})    

@mod_users.route('/<int:id>', methods=['DELETE'])
@inject.autoparams()
def delete_user(id:int, user_service: UserService, db: SQLAlchemy):
    """
    Delete user
    ---
    tags:
      - Users
    parameters:
      - name: id
        in: path
        required: true
        type: number

    responses:
      200:
        schema:
            type: object
            properties:
                deletedRows:
                    type: number
                    description: The number of deleted rows.
        description: The number of deleted rows.
    """
    deleted_rows=user_service.delete(id)
    db.session.commit()
    return jsonify({"deleted_rows":deleted_rows})    
