import unittest

import inject
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_testing import TestCase

from .. import app
from .user_model import UserModel


class TestUsers(TestCase):
    db = inject.attr(SQLAlchemy)
    TESTING = True
    def create_app(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:@localhost/cluebanks_test'
        self.db.create_all()
        self._delete_all_users()
        return app

    def _create_five_users(self):
        self.db.session.add(UserModel(firstname="François-Xavier", lastname="De Nys"))
        self.db.session.add(UserModel(firstname="Jean", lastname="Dupont"))
        self.db.session.add(UserModel(firstname="Albert", lastname="Durant"))
        self.db.session.add(UserModel(firstname="Joseph", lastname="Duchmol"))
        self.db.session.add(UserModel(firstname="Olivier", lastname="Truc"))
        self.db.session.commit()
    
    def _create_a_user(self):
        user=UserModel(firstname="François-Xavier", lastname="De Nys")
        self.db.session.add(user)
        self.db.session.commit()
        return user

    def _delete_all_users(self):
        self.db.session.query(UserModel).delete()
        self.db.session.commit()

    def test_get_users(self):
        self._create_five_users()
        response = self.client.get("/users")   
        assert response.status_code == 200   
        users=response.get_json()
        assert users
        assert len(users)==5
        assert users[0]['firstname']=="François-Xavier"
        assert users[4]['lastname']=="Truc"
        self._delete_all_users()

    def test_get_users__with_offset_parameter(self):
        self._create_five_users()
        response = self.client.get("/users?offset=2")   
        assert response.status_code == 200   
        users=response.get_json()
        assert users
        assert len(users)==3
        assert users[0]['firstname']=="Albert"
        assert users[2]['lastname']=="Truc"
        self._delete_all_users()

    def test_get_users_with_limit_parameter(self):
        self._create_five_users()
        response = self.client.get("/users?limit=2")   
        assert response.status_code == 200   
        users=response.get_json()
        assert users
        assert len(users)==2
        assert users[0]['firstname']=="François-Xavier"
        assert users[1]['lastname']=="Dupont"
        self._delete_all_users()

    def test_get_users_with_limit_and_offset_parameter(self):
        self._create_five_users()
        response = self.client.get("/users?offset=1&limit=1")   
        assert response.status_code == 200   
        users=response.get_json()
        assert users
        assert len(users)==1
        assert users[0]['firstname']=="Jean"
        self._delete_all_users()

    def test_get_user(self):
        user=self._create_a_user()
        print ("user.id===========",user.id)
        response = self.client.get("/users/"+str(user.id))   
        assert response.status_code == 200   
        user=response.get_json()
        assert user
        assert user['firstname']=="François-Xavier"
        assert user['lastname']=="De Nys"
        self._delete_all_users()

    def test_post_user_without_lastname(self):
        response=self.client.post("/users", json={"firstname":"François-Xavier"})
        assert response.status_code == 500   

    def test_post_user_without_firstname(self):
        response=self.client.post("/users", json={"lastname":"De Nys"})
        assert response.status_code == 500   

    def test_post_user(self):
        response=self.client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys"})
        assert response.status_code == 200   
        user = UserModel.query.filter(UserModel.id==response.get_json()['id']).first()
        assert user.firstname == "François-Xavier" 
        assert user.lastname == "De Nys" 

    def test_post_user_with_address(self):
        response=self.client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys", "address":"Kamerdelle 30"})
        assert response.status_code == 200   
        user = UserModel.query.filter(UserModel.id==response.get_json()['id']).first()
        assert user.lat >= 50.801 and user.lat <= 50.802
        assert user.lng >= 4.348 and user.lng <= 4.349

# Too be continued....