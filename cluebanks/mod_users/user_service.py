from typing import Union

import inject
from flask import jsonify, request
from typeguard import typechecked
from flask_sqlalchemy import SQLAlchemy

from .user_schemas import CreateUserSchema, SetUserSchema
from .user_model import UserModel
from ..geocoder.geocoder_service import GeocoderService
from ..my_exceptions import ClueBanksException

class UserService:
    db = inject.attr(SQLAlchemy)
    geocoder_service = inject.attr(GeocoderService)

    @typechecked
    def list(self, offset: Union[int,None], limit: Union[int,None]):
        return self.db.session.query(UserModel).offset(offset).limit(limit).all()
 
    @typechecked
    def get(self, id:int):
        return self.db.session.query(UserModel).filter(UserModel.id==id).one()

    @typechecked
    def create(self, user: CreateUserSchema):
        user = UserModel(**user.dict())  
        if user.address:
            g=self.geocoder_service.geocode(user.address)
            user.lat=g['lat']
            user.lng=g['lng']
        self.db.session.add(user)
        return user

    @typechecked
    def set(self, id: int, user: SetUserSchema):
        if user.address:
            g=self.geocoder_service.geocode(user.address)
            user.lat=g['lat']
            user.lng=g['lng']
        return self.db.session.query(UserModel).filter(UserModel.id==id).update(user.dict())

    @typechecked
    def delete(self, id: int):
        user=self.db.session.query(UserModel).filter(UserModel.id==id).with_for_update().one()
        for account in user.accounts:
            if account.balance>0:
                raise ClueBanksException('User accounts are not empty')
        return self.db.session.query(UserModel).filter(UserModel.id==id).delete()
