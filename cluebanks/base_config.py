class BaseConfig:
    SWAGGER = { "title" : "ClueBanks", "description": "A proof of concept for senior backend developer application at CluePoints."}
    SQLALCHEMY_TRACK_MODIFICATIONS = True 
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_recycle":60}

