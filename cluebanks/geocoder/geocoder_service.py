import json
from urllib.parse import urlencode
from urllib.request import urlopen

from flask import current_app
from typeguard import typechecked
from ..my_exceptions import ClueBanksException


class GeocoderService:
    @typechecked
    def geocode(self, address:str):
        try:
            response = urlopen('https://maps.googleapis.com/maps/api/geocode/json?%s' %urlencode({"address":address, "key": current_app.config['GOOGLE_API_KEY']}))
        except:
            raise ClueBanksException("Google geocoding API did not respond correctly")
        finally:
            if (response.status!=200):
                raise ClueBanksException("Google geocoding API did not respond correctly")
            else:
                response = json.loads(response.read().decode('utf-8'))
                if (response['status']=='OK'):
                    return response['results'][0]['geometry']['location']
                elif (response['status']=='ZERO_RESULTS'):
                    raise ClueBanksException("Address is not a valid address")
                elif (response['status']=='REQUEST_DENIED'):
                    raise ClueBanksException('Invalid GOOGLE_API_KEY')
                else:
                    raise ClueBanksException(response['status'])
