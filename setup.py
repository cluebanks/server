from setuptools import setup

setup(
    name='cluebanks',
    packages=['cluebanks'],
    include_package_data=True,
    install_requires=[
        'flask',
        'SQLAlchemy',
        'mysql-connector',
        'sqlalchemy_serializer',
        'flask_sqlalchemy',
        'inject',
        'pydantic',
        'flasgger',
        'pytest',
        'flask_testing'
    ],
)