# ClueBanks

A proof of concept for senior backend developer application at CluePoints.

# Description
You are working for a small bank that is willing to setup a homebanking system. They
decided to go for a Rest API as they want to build a cross device system, accessible
from tablets, smartphones, etc .

The information that they are willing to store are:
* Account type (Credit card, etc)
* Account number
* Current account balance
* Client’s name
* Client’s first name
* Client’s date of birth
* Client’s address
* Client’s address’ coordinates (latitude & longitude)

### Your task
Write a small web application that exposes a few REST API and allows users to interact
with the different entities. The requirements for the MVP are:
* must implement the following endpoints:
* List users
* Create a new user
* Update an existing user
– List accounts
– Create a new account
* must implement the following features:
* Transfer money from 1 account to another reliably
* The adress' coordinates must be computed by your application whenever a
user’s address is entered/modified

### Guidelines
* The programming language for the RESTful API must be Python
* All routes must return JSON response, no frontend/html is expected
* Authentication of user is not required
* Keep best development practices in mind
* You can choose the framework and the database type but make sure to pick
technologies that fits the need
* Avoid using a rest framework (e.g : you may use django or flask but not djangorest-
framework or flask-restful)
* We will add you as a contributor of a Git repository (on gitlab.com)

### Swagger

http://localhost/apidocs/

# Usage
## Users
### Create user
POST /users
```
{
    firstname: string                   The user's firstname.
    lastname : string                   The user's lastname.
    address : Optional[string]          The user's address.
    birthdate : Optional[string]        The user's birthdate ('YYYY-MM-DD').
}
```
### Modify user
PUT /users
```
{
    firstname: Optional[string]         The user's firstname.
    lastname : Optional[string]         The user's lastname.
    address : Optional[string]          The user's address.
    birthdate : Optional[string]        The user's birthdate ('YYYY-MM-DD').
}
```
### Delete user
DELETE /users/{user_id}  
All user accounts must be empty
### Get user list
GET /users?offset={offset}&limit={limit}
### Get a single user
GET /users/{user_id}
## Accounts
### Create account
POST /accounts
```
{
    account_type: AccountType           The account type (silver, gold, platinium)
    user_id: int                        The user_id of the accounts owner.
}
```
Returns a account number.
### Credit account
POST /accounts/{number}/credit
```
{
    amount: int                         The amount to be credited.
}
```
### Debit account
POST /accounts/{number}/debit
```
{
    amount: int                         The amount to be credited.
}
```
### Transfer money 
POST /accounts/{number}/transfer-to/{number}
```
{
    amount: int                         The amount to be transfered.
}
```
### Delete account
DELETE /accounts/{user_id}  
Account must be empty
### Get account list
GET /accounts?offset={offset}&limit={limit}
### Get a single account
GET /accounts/{number}
